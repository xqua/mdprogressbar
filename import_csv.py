#!/usr/bin/env python

import pandas as pd

df = pd.read_csv("https://docs.google.com/spreadsheets/d/e/2PACX-1vQYfZiCjc3kiPhirH2srQASI-XEiem-WxoFe9BYipsqddTDdOHACTU8brhKmpZ_wgxdUEhVjj3ygW7Y/pub?gid=0&single=true&output=csv", header=4)

def makeJson(row):
    res = '{{startDate: "{}", endDate: "{}", notstarted: "{}", finished: "{}", current: "{}"}},'.format(row['Start date'], row['End date'], row['Label for not yet started'], row['Label when finished'], row['Label when ongoing'])
    return(res)

arr = df.apply(makeJson, axis=1)

f = open("./src/bars.js", 'w')
f.write("""let barsTable = [
{}
]

export default barsTable;
""".format("\n".join(arr)))
f.close()

let barsTable = [
{startDate: "August 3, 2020 8:00:00", endDate: "December 20, 2020 8:00:00", notstarted: "First semester not started", finished: "You survived the First semester", current: "First semester is on fire"},
{startDate: "January 4, 2021 8:00:00", endDate: "June 30, 2021 8:00:00", notstarted: "Second semester not started", finished: "You survived the Second semester", current: "Second semester is on fire"},
{startDate: "August 3, 2020 8:00:00", endDate: "August 5, 2020 8:00:00", notstarted: "This won't show", finished: "This is the example", current: "This won't show"},
{startDate: "September 5, 2020 8:00:00", endDate: "September 9, 2020 8:00:00", notstarted: "This is the example", finished: "This won't show", current: "This won't show"},
{startDate: "August 3, 2020 8:00:00", endDate: "September 5, 2020 8:00:00", notstarted: "This won't show", finished: "This won't show", current: "This is the example"},
]

export default barsTable;

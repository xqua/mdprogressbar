import React, {useEffect, useState} from 'react';
import 'semantic-ui-css/semantic.min.css'
import { Progress } from 'semantic-ui-react'

let notStartedColor = "grey"
let currentColor = "blue"
let finishedColor = "olive"

function Bar(props) {
  const [currentTime, setCurrentTime] = useState(0);
  const [progress, setProgress] = useState(0);
  const [color, setColor] = useState("grey");
  const [label, setLabel] = useState("");

  useEffect(() => {
    const startDate = new Date(props.startDate);
    const endDate = new Date(props.endDate);
    if (currentTime < startDate) {
      setProgress(0);
      setColor(notStartedColor);
      setLabel(props.notstarted)
    }
    else if (currentTime > endDate) {
      setProgress(100);
      setColor(finishedColor);
      setLabel(props.finished)
    }
    else {
      setProgress(((currentTime - startDate) / (endDate -startDate)) * 100);
      setColor(currentColor);
      setLabel(props.current)
    }
  }, [currentTime, props])

  useEffect(() => {
  // Update the document title using the browser API
    const interval = setInterval(() => {
      const currentTime = Date.now();
      setCurrentTime(currentTime);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <Progress percent={progress} color={color} label={label}/>
  );
}

export default Bar;

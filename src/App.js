import React, {useEffect, useState} from 'react';
import './App.css';
import Bar from './Bar.js';
// import statsTable from './stats.js'
import barsTable from './bars.js'
import logo from './cropped-logomed-large.jpg'
import 'semantic-ui-css/semantic.min.css'
import { Progress, Container, Header, Statistic } from 'semantic-ui-react'

let startDate = new Date('August 3, 2020 08:00:00');
let endDate = new Date('June 30, 2024 08:00:00');
//
// function interpolate(week, metric) {
//   const value1 = statsTable[Math.round(week)][metric];
//   const value2 = statsTable[Math.round(week) + 1][metric];
//   const value = value1 + (week - Math.round(week)) * ((value2 - value1) / ((Math.round(week) + 1) - Math.round(week)))
//   return value
// }

function sortbars(a, b) {
  const aDate = new Date(a.endDate)
  const bDate = new Date(b.endDate)
  let comparison = 0;
  if (aDate > bDate) {
    comparison = 1;
  } else if (aDate < bDate) {
    comparison = -1;
  }
  return comparison;
}

function App() {

  const [bars, setBars] = useState([]);
  const [progress, setProgress] = useState(0);
  const [daysElapsed, setDaysElapsed] = useState(0);
  const [weeksElasped, setWeeksElapsed] = useState(0);



  useEffect(() => {
    const bars = barsTable.slice().sort(sortbars);
    console.log(bars);
    setBars(bars);
  }, [])

  useEffect(() => {
  // Update the document title using the browser API
    const interval = setInterval(() => {
      const now = Date.now();
      setProgress(((now-startDate) / (endDate-startDate)) * 100)
      const timeElapsed = now - startDate;
      const daysElapsed = timeElapsed / (1000 * 3600 * 24);
      setDaysElapsed(daysElapsed);
      const weeksElasped = daysElapsed / 7
      setWeeksElapsed(weeksElasped);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className="root">
    <Container textAlign="center" className="App-header">
    <img src={logo} alt="logo"/>
      <Header
        as='h1'
        inverted
        style={{
          fontSize: '2em',
          fontWeight: 'normal',
          marginBottom: 0,
        }}
      >Medical Doctors in preparation</Header>
      <Progress percent={progress} color='violet' size='big' className="mainProgress">{progress.toFixed(5)}%</Progress>
      <Header as="h2" style={{color: "white"}}>Knowledge is compiling... Please wait!</Header>
    </Container>
    <Container className="App-stats">
      <Statistic.Group>
      <Statistic>
        <Statistic.Value>{Math.round(daysElapsed)}</Statistic.Value>
        <Statistic.Label>Days since hell started</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{Math.round(weeksElasped)}</Statistic.Value>
        <Statistic.Label>Current Week</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{Math.round(weeksElasped)}</Statistic.Value>
        <Statistic.Label>Add some stats</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{350}</Statistic.Value>
        <Statistic.Label>Like number of facts known</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{485}</Statistic.Value>
        <Statistic.Label>Or number of coffees</Statistic.Label>
      </Statistic>
      </Statistic.Group>
      {
        bars.map((value, index) => {
          return (
            <Bar
              key={index}
              startDate={value.startDate}
              endDate={value.endDate}
              notstarted={value.notstarted}
              finished={value.finished}
              current={value.current}
            />
          )
        })
      }
    </Container>
    </div>
  );
}

export default App;
